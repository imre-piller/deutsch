Névmások
========

Személyes névmások
------------------

.. csv-table::
    :header: N,A,D,G
    
    ich,mich,mir,meiner
    du,dich,dir,deiner
    er,ihn,ihm,seiner
    sie,sie,ihr,ihrer
    es,es,ihm,seiner
    wir,uns,uns,unser
    ihr,euch,euch,euer
    sie,sie,ihnen,ihrer


Birtokos névmások
-----------------

.. csv-table::
    :header: M,F,N,P
    
    mein,meine,mein,meine
    dein,deine,dein,deine
    sein,seine,sein,seine
    ihr,ihre,ihr,ihre
    sein,seine,sein,seine
    unser,unsere,unser,unsere
    eure,eure,euer,eure
    ihre,ihre,ihr,ihre
    Ihr,Ihre,Ihr,Ihre


Kérdő névmások
--------------

.. csv-table::
    :header:névmás,jelentés
    
    wer?,ki? kik?
    wen?,kit? kiket?
    wem?,kinek? kiknek?
    wessen?,kinek a ...je? kiknek a ...je?
    was?,mi? mik? mit? miket?
    welcher? welche? welches?,melyik?
    was für ein/eine ...?,milyen?


Névmási határozószók
--------------------

.. csv-table::
    :header: elöljárószó,mutató,jelentés,kérdő vagy vonatkozó,jelentés
    :delim: ;
    
    an;daran, hieran;erre, arra, ezen, azon;woran(?);mire? min? amire, amin
    auf;darauf, hierauf;erre, arra, ezen, azon;worauf(?);mire? min? amire, amin
    aus;daraus, hieraus;ebből, abból;woraus(?);miből? amiből
    bei;dabei, hierbei;emellett, amellett, ennél, annál;wobei(?);mi mellett? minél? aminél
    durch;dadurch, hierdurch;ezáltal, azáltal;wodurch(?);mi által? ami által, amivel
    für;dafür, hierfür;ezért, azért;wofür(?);miért? amiért
    gegen;dagegen;ez ellen, az ellen;wogegen(?);mi ellen? ami ellen
    hinter;dahinter;emögött, amögött, emögé, amögé;wohinter(?);mi mögött? ami mögött
    in;darin, darein;ebben, abban, ebbe, abba;worin(?);miben? amiben, mibe? amibe
    mit;damit, hiermit;ezzel, azzal;womit(?);mivel? amivel
    nach;danach, hiernach;ezután, azután;wonach(?);mi után? ami után
    neben;daneben, hierneben;emellett, amellett, emellé, amellé;woneben(?);mi mellett? ami mellett
    um;darum;ezért, azért;worum(?);miért? amiért
    unter;darunter, hierunter;ez alatt, az alatt, ez alá, az alá;worunter(?);mi alatt? ami alatt
    über;darüber, hierüber;efölött, afölött, erről, arról, efölé, afölé;worüber(?);mi fölött? miről? ami fölött, amiről
    von;davon, hiervon;ettől, attól;wovon(?);mitől? amitől
    vor;davor, hiervor;ez elé, az elé, ez előtt, az előtt;wovor(?);mi elé mi előtt? ami elé, ami előtt
    zu;dazu, hierzu;ehhez, ahhoz;wozu(?);mihez?
    zwischen;dazwischen;e közé, a közé, e között, a között;wozwischen(?);mi közé? mi között? ami közé, ami között

    