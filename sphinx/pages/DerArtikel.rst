Névelő
======

Határozott névelő
-----------------

.. csv-table::
    :header: ,M,F,N,P
    
    N,der,die,das,die
    A,den,die,das,die
    D,dem,der,dem,den
    G,des -es,der,des -es,der


Így ragozhatók még az alábbi névmások:

* *dieser*: ez, ezek
* *jener*: az, azok
* *solcher*: ilyen, olyan
* *jeder*: mindegyik, mindenki, minden
* *aller*: minden, egész, összes, mindnyájan
* *welcher*: Melyik?
* *mancher*: némely, nem egy, sok


Határozatlan névelő
-------------------

.. csv-table::
    :header: ,M,F,N,P
    
    N,ein,eine,ein,-
    A,einen,eine,ein,-
    D,einem,einer,einem,-
    G,eines -es,einer,eines -es,-

|

.. csv-table::
    :header: ,M,F,N,P
    
    N,kein,keine,kein,keine
    A,keinen,keine,kein,keine
    D,keinem,keiner,keinem,keinen
    G,keines,keiner,keines,keiner


Így ragozzuk a birtokos névmásokat is.
