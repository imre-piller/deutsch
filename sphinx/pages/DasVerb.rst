Igeidők
=======

Präsens
-------

Jelen idő

Präteritum
----------

Imperfekt, elbeszélő múlt, egyszerű múlt, folyamatos múlt

Perfekt
-------

Összetett múlt, beszélt múlt

Plusquamperfekt
---------------

Régmúlt, befejezett múlt

Futur I
-------

Jövő idő

Futur II
--------

Befejezett jövő idő
