.. deutsch documentation master file, created by
   sphinx-quickstart on Thu Aug 29 18:43:30 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Deutsch
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/DerArtikel.rst
   pages/DasPronomen.rst
   pages/DasVerb.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
